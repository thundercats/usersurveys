<?php


class Section extends DataObject implements UserSurveysFormField
{
    use EditableFieldHasChildren;

    private static $db = array(
        'Title' => 'Varchar',
        'SortOrder' => 'Int',
    );

    private static $has_one = array(
        'Form' => 'SurveyUserDefinedForm',
    );

    private static $has_many = array(
        'Questions' => 'EditableFormField',
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // tidy up
        $fields->removeByName('FormID');
        $fields->removeByName('SortOrder');

        // configure gridfield
        $questions = $fields->fieldByName('Root.Questions.Questions');
        if ($questions) {
            /* @var $questions GridField */
            $questionsConfig = $questions->getConfig();
            $questionsConfig->addComponent(new GridFieldSortableRows('Sort'));
            $questionsConfig->removeComponentsByType('GridFieldAddNewButton');

            $select = new GridFieldAddNewMultiClass();
            $select->setClasses($this->getSupportedChildrenTypes());
            $questionsConfig->addComponent($select);

            // Move questions gridfield to main tab so we don't have to change tabs so often
            $fields->removeByName('Questions');
            $fields->addFieldToTab('Root.Main', $questions);
            $fields->insertBefore('Questions', new HeaderField('QuestionsHeader', 'Section Questions'));
        }

        return $fields;
    }

    public function getFieldDefinition()
    {
        $questions = [];
        foreach ($this->Questions()->sort('Sort') as $question) {
            /* @var $question EditableFormField */
            $questions[] = $question->getFieldDefinition();
        }

        $def = [
            'title' => $this->Title,
            'showOnLoad' => (boolean)$this->InitialVisibility,
            'introduction' => '',
            'questions' => $questions,
        ];

        $this->extend('updateFieldDefinition', $def);

        return $def;
    }

    /**
     * {@inheritdoc}
     */
    public function getLongTitle()
    {
        return $this->Title;
    }

    public function getForm()
    {
        return $this->Form();
    }

}
