<?php

class SurveyContactInformation extends DataObject implements UserSurveysJsonSerializable
{
    private static $db = array(
        'Title' => 'Varchar(255)',
        'FreePhone' => 'Varchar(255)',
        'Phone' => 'Varchar(255)',
        'FreeFax' => 'Varchar(255)',
        'Fax' => 'Varchar(255)',
        'Email' => 'Varchar(255)',
        'Mail' => 'Text',
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        return $fields;
    }

    /**
     * Return an associative array of db field names and their values.
     *
     * @return array
     */
    public function getFieldDefinition()
    {
        if (!$this->ID) {
            return false;
        }

        $data = array();
        foreach (self::$db as $field => $type) {
            if ($field !== 'Title' && !empty($this->$field)) {
                $data[$field] = str_replace(array("\r\n", "\r", "\n"), '<br>', $this->$field);
            }
        }

        return $data;
    }
}
