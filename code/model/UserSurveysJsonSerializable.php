<?php


/**
 * This interface marks items which can be serialised into JSON form schema
 *
 * We would have used the built-in JsonSerializable interface but it doesn't let us add interfaces
 * to objects at run time.
 *
 * If we add the interface to our EditableFormFieldExtension class, and apply that extension to
 * EditableFormField, when we call json_encode($field) it does not call the jsonSerialize method added
 * by the extension.
 *
 * So we have to create our own interface and use our own method names, and this is that interface.
 */
interface UserSurveysJsonSerializable {

    /**
     * @return array - nested array of data to be encoded as json which represents this field
     */
    public function getFieldDefinition();

}
