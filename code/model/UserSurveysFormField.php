<?php

interface UserSurveysFormField extends UserSurveysJsonSerializable
{
    /**
     * Returns a long title meant to be optimised for search in dropdown menu's
     *
     * @return string
     */
    public function getLongTitle();
}
