<?php

/**
 * Records which represents an individual display rule.
 */
class UserSurveysDisplayRule extends DataObject implements UserSurveysJsonSerializable
{
    private static $db = [
        'Values' => 'Text',
    ];

    private static $has_one = [
        'Field' => 'EditableFormField',
        'Parent' => 'DataObject',
    ];

    private static $singular_name = 'Display Rule';

    private static $plural_name = 'Display Rules';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // Has a strange sorting bug - need to remove then re-add to put things
        // in the correct order.
        $fields->removeByName('Values');

        $valuesDescription = 'Values are case sensitive, separated by new lines. '
            . 'You can prefix each value by # character for readability.';

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('FieldID', 'When field', $this->getQuestions()),
            TextareaField::create('Values', 'equals to one of these values')
                ->setDescription($valuesDescription),
        ]);

        return $fields;
    }

    public function getFieldDefinition()
    {
        $definition = [
            'name' => $this->Field() ? $this->Field()->getKey() : '',
            'values' => $this->Values,
        ];

        $this->extend('updateFieldDefinition', $definition);

        // If either name or value is empty then we return an empty array
        if(empty($definition['name']) || empty($definition['values'])) {
            return [];
        }

        return $definition;
    }

    /**
     * @return array - array of field name => title
     */
    protected function getQuestions()
    {
        if(!$this->Parent()) {
            $currentPage = Session::get('CMSMain.currentPage');
            $form = SurveyUserDefinedForm::get()->byId($currentPage);
        } else {
            $form = $this->Parent()->getForm();
        }

        /* @var $form SurveyUserDefinedForm */
        if (!$form) {
            return [];
        }

        // Our map includes (unique field name) so it is easier for
        // questionnaire designers to find
        $map = [];
        foreach ($form->getAllFields() as $field) {
            $map[$field->ID] = $field->getLongTitle();
        }

        return $map;
    }
}
