<?php

class SurveyUserDefinedForm extends UserDefinedForm implements UserSurveysJsonSerializable
{
    private static $defaults = array(
        'Content' => '$SurveyUserDefinedForm',
    );

    private static $has_one = array(
        'ContactInformation' => 'SurveyContactInformation',
    );

    private static $has_many = array(
        'Sections' => 'Section',
    );

    /**
     * @return SurveyPage
     */
    public function Survey()
    {
        return $this->getParent();
    }

    public function getSubmitURL()
    {
        return $this->Survey()->submitURL();
    }

    public function getSubmitMethod()
    {
        return $this->Survey()->submitMethod();
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // Include js to resolve JIRA issue CEN-128
        Requirements::javascript('usersurveys/javascript/LeftAndMain.UserSurveys.js');

        // Remove unnecessary tabs, relating to standard UDF submission
        $fields->removeFieldsFromTab('Root', array(
            'Recipients',
            'Submissions',
            'Translations',
            'Fields',
        ));

        // This is all user defined forms stuff we don't need anymore
        $fields->removeByName('FormOptions');

        $gridFieldConfig = new GridFieldConfig_RecordEditor();
        $gridFieldConfig->addComponent(new GridFieldSortableRows('SortOrder'));
        $gridField = new GridField('NewFormFieldsGrid', 'Sections', $this->Sections(), $gridFieldConfig);

        // Make sure the GridField is at the start of the section
        $tab = $fields->findOrMakeTab('Root.FormFields');
        $tab->unshift($gridField);

        return $fields;
    }

    public function doProcess($data, $form)
    {
        // Disable standard UDF submission for now
        // @todo Implement call to SubmissionService
        $link = Controller::join_links(Controller::curr()->Link(), '#finished');

        return Controller::curr()->redirect($link);
    }

    /**
     * On after publish, the following tasks are invoked:.
     *
     *  - Save the form's definition to a json file
     *
     * @TODO Delete an old file when the page's url segment has changed
     */
    public function onAfterPublish()
    {
        $file = $this->generateJsonFile();

        if(class_exists('VersionedFileExtension')) {
            $file->createVersion();
        }

        $this->extend('onAfterGenerateJsonFile', $file);

        $file->write();
    }

    public function generateJsonFile()
    {
        //find or make File database object for JSON file
        $file = $this->getFormDefinitionJsonFile();

        //full filesystem path of the file
        $filepath = $file->getFullPath();

        //obtain JSON representation of data
        $niceData = json_encode($this->getFieldDefinition(), JSON_PRETTY_PRINT);

        // Write the JSON into a file on the filesystem
        file_put_contents($filepath, $niceData);

        return $file;
    }

    public function getFormDefinitionJsonFile() {
        $name = $this->URLSegment . '.json';
        $file = File::get()->find('Name', $name);
        if(!$file) {
            //create folder if it doesn't exist
            $folder = Folder::find_or_make('/json/');
            //create File dataobject if it doesn't exist
            $file = File::create();
            $file->Title = $name;
            $file->Filename = $name;
            $file->ParentID = $folder->ID;
            $file->write();
        }

        return $file;
    }

    /**
     * Return a file path for the form definition json file. The file name is
     * based on the page's url segment.
     *
     * @return string
     */
    public function getFormDefinitionJsonFilePath()
    {
        $file = $this->getFormDefinitionJsonFile();

        $name = $file->Name;

        return $file->getFullPath();
    }

    public function getFormDefinitionJsonFileUrl()
    {
        $file = $this->getFormDefinitionJsonFile();

        return $file->getAbsoluteURL();
    }

    public function getFieldDefinition()
    {
        $sections = array();
        foreach($this->Sections()->sort('SortOrder') as $section) {
            /* @var $section Section */
            $sections[] = $section->getFieldDefinition();
        }

        $data = array(
            'name' => $this->Title,
            'contact' => $this->ContactInformation()->getFieldDefinition(),
            'sections' => $sections,
        );

        $this->extend('updateJSONFieldDefinition', $data);

        return $data;
    }

    /**
     * @return array of EditableFormField
     */
    public function getAllFields($node = null) {
        if ($node === null) {
            // Initial call, get fields under this page's sections
            $sectionIDs = array_map(
                function(Section $section) {
                    return $section->ID;
                },
                $this->Sections()->toArray()
            );
            $sectionFields = EditableFormField::get()->filter('SectionID', $sectionIDs);

            $allFields = [];
            foreach ($sectionFields as $sectionField) {
                /* @var $sectionField EditableFormField */
                $allFields = array_merge($allFields, $this->getAllFields($sectionField));
            }
            return $allFields;

        } else if ($node instanceof EditableFormFieldGroup) {
            // Recursive call, get nested fields under this group
            $group = $node; /* @var $group EditableFormFieldGroup */
            $childFields = [];
            foreach ($group->ChildFields()->toArray() as $childField) {
                $childFields = array_merge($childFields, $this->getAllFields($childField));
            }
            return $childFields;

        } else {
            // Leaf of tree, return self
            return [$node];
        }
    }
}

class SurveyUserDefinedForm_Controller extends UserDefinedForm_Controller
{
    private static $allowed_actions = array(
        'index',
        'ping',
        'Form',
        'finished',
    );
    /**
     * Using $SurveyUserDefinedForm in the Content area of the page shows
     * where the form should be rendered into. If it does not exist
     * then default back to $Form.
     *
     * @return array
     */
    public function index()
    {
        if ($this->Content && $form = $this->Form()) {
            $hasLocation = stristr($this->Content, '$SurveyUserDefinedForm');
            if ($hasLocation) {
                $content = preg_replace('/(<p[^>]*>)?\\$SurveyUserDefinedForm(<\\/p>)?/i', $form->forTemplate(), $this->Content);

                return array(
                    'Content' => DBField::create_field('HTMLText', $content),
                    'Form' => '',
                );
            }
        }

        return array(
            'Content' => DBField::create_field('HTMLText', $this->Content),
            'Form' => $this->Form(),
        );
    }

    /**
     * Get the form for the page. Form can be modified by calling {@link updateForm()}
     * on a UserDefinedForm extension.
     *
     * @return Forms
     */
    public function Form()
    {
        return null;
    }
}
