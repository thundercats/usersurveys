<?php

class UserSurveysEditableCheckboxGroupField extends EditableCheckboxGroupField
{
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // We can't edit options until the field is saved.
        if(!$this->ID) {
            $fields->removeByName('Options');
        }

        return $fields;
    }
}
