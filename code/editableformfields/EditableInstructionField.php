<?php
/**
 * Allows an editor to insert a rich text block with a question number
 */

class EditableFormInstruction extends EditableFormField implements UserSurveysFormField
{

	private static $singular_name = 'Instruction';

	private static $plural_name = 'Instructions';

	private static $literal = true;

	private static $db = array(
		'HideFromReports' => 'Boolean(0)' // from CustomSettings
	);

	private static $defaults = array(
		'HideFromReports' => false
	);

	/**
	 * @return FieldList
	 */
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->removeByName(array('Default', 'Validation', 'RightTitle'));
		$fields->addFieldsToTab('Root.Main', array(
			CheckboxField::create(
				'HideFromReports',
				_t('EditableLiteralField.HIDEFROMREPORT', 'Hide from reports?')
			)
		));

		return $fields;
	}

	public function showInReports() {
		return !$this->HideFromReports;
	}

	public function getFieldValidationOptions() {
		return false;
	}

	public function getSelectorHolder() {
		return "$(\":header[data-id='{$this->Name}']\")";
	}

	public function getFieldDefinition()
	{
		$def = parent::getFieldDefinition();
		$def['type'] = 'instruction';
		return $def;
	}

    public function getLongTitle()
    {
        // calls magic __get
        return parent::getLongTitle();
    }
}
