<?php

class EditableAutosuggestField extends EditableFormField implements UserSurveysFormField
{
    private static $singular_name = 'Auto-Suggest';

    private static $plural_name = 'Auto-Suggests';

    private static $db = array(
        'SourceFile' => 'Varchar',
    );

    // removed slash before assets to suit for case of site URL not equals to base URL
    public static $json_source_files_folder = 'assets/jsonsourcefiles';

    private static function get_json_directory()
    {
        return Director::baseFolder().'/'.self::$json_source_files_folder;
    }

    private static function get_json_files()
    {
        if (!file_exists(self::get_json_directory())) {
            Filesystem::makeFolder(self::get_json_directory());
            Filesystem::sync();
        }

        $files = scandir(self::get_json_directory());
        $opts = [];
        foreach ($files as $val) {
            if ($val !== '.' && $val !== '..' && is_file(self::get_json_directory().'/'.$val)) {
                $opts[$val] = $val;
            }
        }

        return $opts;
    }

    public function getCMSFields()
    {
        $this->beforeUpdateCMSFields(function (FieldList $fields) {
            $dropdown = DropdownField::create('SourceFile', 'Source file', self::get_json_files());

            // Display info text where to upload the source file to if there
            // isn't one
            if (count(self::get_json_files()) < 1) {
                $folderName = str_replace('/assets/', '', EditableAutosuggestField::$json_source_files_folder);
                $dropdown->setRightTitle('Please upload a source JSON file under <strong>'.$folderName.'</strong> folder in <strong>Files</strong> section.');
            }

            $fields->addFieldToTab('Root.Main', $dropdown);
        });

        return parent::getCMSFields();
    }

    public function getFieldDefinition()
    {
        $def = parent::getFieldDefinition();
        $def['type'] = 'autosuggest';
        $def['sourceUrl'] = EditableAutosuggestField::$json_source_files_folder . '/' . $this->SourceFile;
        return $def;
    }

    public function getLongTitle()
    {
        // calls magic __get method
        return parent::getLongTitle();
    }

}
