<?php

class PlaceHolderField extends EditableFormField implements UserSurveysFormField
{
    private static $singular_name = 'Placeholder';

    private static $plural_name = 'Placeholder';

    private static $literal = true;

    public function getFieldDefinition()
    {
        $def = parent::getFieldDefinition();
        $def['type'] = 'placeholder';
        return $def;
    }

    public function getLongTitle()
    {
        // Calls magic __get
        return parent::getLongTitle();
    }

}
