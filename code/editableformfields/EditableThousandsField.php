<?php


class EditableThousandsField extends EditableNumericField implements UserSurveysFormField
{

    private static $singular_name = 'Thousands field';

    private static $plural_name = 'Thousands fields';

    public function getFieldDefinition()
    {
        $definition = parent::getFieldDefinition();
        $definition['type'] = 'text-number';
        $definition['thousands'] = true;
        return $definition;
    }

    public function getLongTitle()
    {
        // calls magic __get
        return parent::getLongTitle();
    }

}
