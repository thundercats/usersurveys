<?php

class EditableNameField extends EditableFormField implements UserSurveysFormField
{
    private static $singular_name = 'Name Field';

    private static $plural_name = 'Names';

    private static $db = array(
        'ShowMiddleName' => 'Boolean',
        'ValidationOption' => "Enum('Both, Either, None', 'Both')",
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $validationOptions = singleton('EditableNameField')->dbObject('ValidationOption')->enumValues();

        $fields->addFieldsToTab('Root.Main', CheckboxField::create('ShowMiddleName', 'Show Middle Name field'), 'Description');
        $fields->addFieldsToTab('Root.Validation', OptionSetField::create('ValidationOption', 'Validation Options', $validationOptions, array_shift($validationOptions))
            ->setDescription('Note: The Middle Name field is always optional.'));

        $fields->removeFieldFromTab('Root.Validation', 'Required');

        return $fields;
    }

    public function getFieldDefinition()
    {
        $def = parent::getFieldDefinition();
        $def['type'] = 'name';
        $def['showMiddleName'] = (bool)$this->ShowMiddleName;
        $def['validationOption'] = strtolower($this->ValidationOption);
        return $def;
    }

    public function getLongTitle()
    {
        // Calls magic __get
        return parent::getLongTitle();
    }

}
