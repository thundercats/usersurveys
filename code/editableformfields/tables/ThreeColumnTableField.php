<?php

class ThreeColumnTableField extends TwoColumnTableField
{
    private static $singular_name = 'Table (3 columns)';

    protected $columnCount = 3;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab(
            'Root.Headers',
            TextField::create('Header3', 'Header 3')
        );

        $fields->addFieldsToTab(
            'Root.Totals', [
            HeaderField::create('Column 3', 'Column 3'),
            OptionsetField::create(
                'Column3Calculate',
                'Calculate totals?',
                [1 => 'Yes', 0 => 'No']
            ),
            TextField::create('Column3UnitLabel', 'Unit Label')
                ->setDescription('Optional')
        ]);


        return $fields;
    }
}
