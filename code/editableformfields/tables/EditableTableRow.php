<?php

class EditableTableRow extends EditableFormFieldGroup
{

    private static $singular_name = 'Table Row';

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $tab = $fields->fieldByName('Root.ChildFields');
        if($tab) {
            $tab->setTitle('Cells');

            $gridField = $fields->dataFieldByName('ChildFields');
            if($gridField) {
                $config = $gridField->getConfig();
                $config->removeComponentsByType('GridFieldAddNewMultiClass');
                $config->removeComponentsByType('GridFieldDeleteAction');

                if($this->getTable()->getColumnCount() <= 2) {
                    $config->removeComponentsByType('GridFieldSortableRows');
                }
            }
        }

        $fields->removeByName('DisplayRules');
        $fields->removeByName('Validation');
        $fields->removeByName('FieldDisplayFormat');

        return $fields;
    }

    public function getFieldDefinition()
    {
        $definition = parent::getFieldDefinition();
        $definition['type'] = 'table-row';

        foreach($definition['fields'] as $name => $field) {
            $definition['fields'][$name]['isTableCell'] = true;
        }

        unset($definition['fieldDisplayFormat']);

        return $definition;
    }

    public function onAfterWrite()
    {
        $cellCount = $this->getCells()->count();
        $requiredCells = $this->getTable()->getColumnCount() - 1;

        $toCreate = $requiredCells - $cellCount;
        while($toCreate > 0) {
            $this->addDefaultField();
            $toCreate--;
        }
    }

    public function getSupportedChildrenTypes()
    {
        return [
            'EditableNumericField' => singleton('EditableNumericField')->singular_name(),
            'EditableTextField' => singleton('EditableTextField')->singular_name(),
            'PlaceHolderField' => singleton('PlaceHolderField')->singular_name()
        ];
    }

    /**
     * @return HasManyList<EditableFormField>
     */
    public function getCells()
    {
        return $this->ChildFields();
    }

    /**
     * @return TwoColumnTableField
     */
    public function getTable()
    {
        return $this->EditableFormFieldGroup();
    }

    protected function addDefaultField()
    {
        $field = EditableTextField::create();
        $field->Title = 'Default Text Field';
        $field->EditableFormFieldGroupID = $this->ID;
        $field->write();
    }

}
