<?php

class TwoColumnTableField extends EditableFormFieldGroup
{
    private static $db = [
        'Header1' => 'Varchar(255)',
        'Header2' => 'Varchar(255)',
        'TotalsLabel' => 'Varchar(255)',
        'Column2Calculate' => 'Boolean',
        'Column2UnitLabel' => 'Varchar(255)',
        // We store extra on this object so we don't have to do joins
        'Header3' => 'Varchar(255)',
        'Column3Calculate' => 'Boolean',
        'Column3UnitLabel' => 'Varchar(255)',
    ];

    private static $singular_name = 'Table (2 columns)';

    protected $columnCount = 2;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Headers', [
            TextField::create('Header1', 'Header 1'),
            TextField::create('Header2', 'Header 2'),
        ]);

        $fields->addFieldsToTab('Root.Totals', [
            TextField::create('TotalsLabel', 'Label'),
            HeaderField::create('Column 2', 'Column 2'),
            OptionsetField::create(
                'Column2Calculate',
                'Calculate totals?',
                [1 => 'Yes', 0 => 'No']
            ),
            TextField::create('Column2UnitLabel', 'Unit Label')
                ->setDescription('Optional')
        ]);

        // Rename child fields to rows
        $tab = $fields->fieldByName('Root.ChildFields');
        if($tab) {
            $tab->setTitle('Rows');
        }

        $fields->removeByName('Validation');
        $fields->removeByName('FieldDisplayFormat');

        return $fields;
    }

    public function getFieldDefinition()
    {
        $definition = parent::getFieldDefinition();
        $definition['type'] = 'table';

        unset($definition['fieldDisplayFormat']);

        if($this->hasHeader()) {
            $definition['headers'] = $this->getTableHeaderCells();
        }

        if($this->hasTotalsRow()) {
            $definition['totalsLabel'] = $this->TotalsLabel;
            $definition['totals'] = $this->getTotalsRow();
        }

        $definition['columnCount'] = $this->getColumnCount();

        return $definition;
    }

    /**
     * @return int
     */
    public function getColumnCount()
    {
        return $this->columnCount;
    }

    public function getRows()
    {
        return $this->ChildFields();
    }

    public function getSupportedChildrenTypes()
    {
        return [
            'EditableTableRow' => singleton('EditableTableRow')->singular_name(),
        ];
    }

    /**
     * Checks if the current table should have a header row
     *
     * @return boolean
     */
    public function hasHeader()
    {
        for ($i = 1; $i <= $this->getColumnCount(); $i++) {
            $name = sprintf('Header%d', $i);
            if(!empty(trim($this->$name))) {
                return true;
            }
        }

        return false;
    }

    /**
     *  Checks whether we should display a totals row
     *
     *  @return boolean
     */
    public function hasTotalsRow()
    {
        for ($i = 2; $i <= $this->getColumnCount(); $i++) {
            $name = sprintf('Column%dCalculate', $i);
            if($this->$name) {
                return true;
            }
        }

        return false;
    }

    /**
     * This will return an array of columns for columns totals if this table
     * has row totals. Otherwise, it will return an empty array.
     *
     * @return array
     */
    public function getTotalsRow()
    {
        $cells = [];

        for($i = 2; $i <= $this->getColumnCount(); $i++) {
            $calcName = sprintf('Column%dCalculate', $i);
            $unitLabelName = sprintf('Column%dUnitLabel', $i);

            $cell = [
                'calculate' => (boolean) $this->$calcName,
                'unitLabel' => $this->$unitLabelName,
            ];

            if(!$cell['calculate'] || empty(trim($cell['unitLabel']))) {
                $cell['unitLabel'] = null;
            }

            $cells[] = $cell;
        }

        return $cells;
    }

    /**
     * Builds an array of data to return as header content
     *
     * @return array
     */
    public function getTableHeaderCells()
    {
        $data = [];
        for ($i = 1; $i <= $this->getColumnCount(); $i++) {
            $name = sprintf('Header%d', $i);
            $data[] = $this->$name;
        }

        return $data;
    }
}
