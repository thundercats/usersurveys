<?php


class EditableFormFieldGroup extends EditableFormField implements UserSurveysFormField
{
    use EditableFieldHasChildren {
        getUnsupportedChildrenTypes as private _getUnsupportedChildrenTypes;
    }

    private static $singular_name = 'Group';

    private static $plural_name = 'Groups';

    private static $db = array(
        'FieldDisplayFormat' => 'enum("Rows, Columns", "Rows")'
    );

    private static $has_many = array(
        'ChildFields' => 'EditableFormField',
    );


    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // If we haven't saved this object, we can't add relations via GridField
        if($this->ID) {
            $gridFieldConfig = new GridFieldConfig_RecordEditor();
            $gridFieldConfig->addComponent(new GridFieldSortableRows('Sort'));

            $select = new GridFieldAddNewMultiClass();
            $select->setClasses($this->getSupportedChildrenTypes());
            $gridFieldConfig->addComponent($select);
            $gridFieldConfig->removeComponentsByType('GridFieldAddNewButton');

            $gridField = new GridField('ChildFields', 'Child Fields', $this->ChildFields(), $gridFieldConfig);
            $fields->addFieldToTab('Root.ChildFields', $gridField);
        }

        $formatDropdown = DropdownField::create(
            'FieldDisplayFormat',
            'Field Display Format',
            $this->dbObject('FieldDisplayFormat')->enumValues());
        $fields->insertAfter('Name', $formatDropdown);

        return $fields;
    }

    protected function getUnsupportedChildrenTypes()
    {
        $unsupported = $this->_getUnsupportedChildrenTypes();

        // Don't allow subclasses of field group
        $subClasses = ClassInfo::subClassesFor('EditableFormFieldGroup');

        return array_merge($unsupported, $subClasses);
    }

    public function getFieldDefinition()
    {
        $definition = parent::getFieldDefinition();
        $definition['type'] = 'composite';
        $definition['fieldDisplayFormat'] = strtolower($this->FieldDisplayFormat);
        $definition['fields'] = [];
        foreach ($this->ChildFields() as $child) {
            /* @var $child EditableFormField */
            $definition['fields'][] = $child->getFieldDefinition();
        }
        return $definition;
    }

    public function getLongTitle()
    {
        // calls magic __get
        return parent::getLongTitle();
    }

}
