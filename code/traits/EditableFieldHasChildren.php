<?php

trait EditableFieldHasChildren
{
    /**
     * Returns a list of question types for the CMS dropdown
     *
     * @return array
     */
    public function getSupportedChildrenTypes()
    {
        $classes = ClassInfo::subClassesFor('EditableFormField');

        $unsupported = $this->getUnsupportedChildrenTypes();
        $finalList = array_filter($classes, function($class) use($unsupported) {
            return !in_array($class, $unsupported) && class_exists($class);
        });

        foreach($finalList as $class) {
            $finalList[$class] = singleton($class)->singular_name();
        }

        return $finalList;
    }

    /**
     * Returns a list of child question types which aren't allowed. This can be
     * overwritten in class implementations.
     *
     * @return array
     */
    public function getUnsupportedChildrenTypes()
    {
        // Load the applications unsupported field types
        $unsupported = Config::inst()->get(get_class($this), 'unsupported_children');
        if(!is_array($unsupported)) {
            $unsupported = [];
        }

        // Hide hidden fields or 'abstract' fields`
        $subclasses = ClassInfo::subClassesFor('EditableFormField');
        foreach($subclasses as $class) {
            $hidden = Config::inst()->get($class, 'hidden');
            $abstract = Config::inst()->get($class, 'abstract', Config::UNINHERITED);
            if($hidden || $abstract) {
                $unsupported[] = $class;
            }
        }

        return $unsupported;
    }
}
