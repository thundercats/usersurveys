<?php

class SurveyContactInformationAdmin extends ModelAdmin
{
    private static $managed_models = 'SurveyContactInformation';

    private static $menu_title = 'Contact Information';

    private static $url_segment = 'survey-contact-information';
}
