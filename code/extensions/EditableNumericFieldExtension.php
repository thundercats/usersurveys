<?php


class EditableNumericFieldExtension extends DataExtension
{
    private static $db = array(
        'UnitLabel' => 'Varchar(255)',
        'AllowDecimals' => 'Boolean',
    );

    public function updateCMSFields(FieldList $fields)
    {
        // Responsive title field for dynamically piping data
        $fields->insertBefore('HelpText',
            TextField::create('UnitLabel', 'Unit Label')
                ->setDescription('Set a unit for the field. (eg. Hectares, Meters, Kilograms)')
        );

        if (!($this->owner instanceof EditableThousandsField)) {
            $fields->insertAfter('UnitLabel', CheckboxField::create('AllowDecimals'));
        }
    }

    public function updateFieldDefinition(&$def)
    {
        $def['type'] = 'text-number';

        // Numeric validation
        if(is_numeric($this->owner->MinValue)) {
            $def['min'] = (int)$this->owner->MinValue;
        }
        if(is_numeric($this->owner->MaxValue)) {
            $def['max'] = (int)$this->owner->MaxValue;
        }

        if(!empty($this->owner->UnitLabel)) {
            $def['unitLabel'] = $this->owner->UnitLabel;
        }

        if ($this->owner->AllowDecimals) {
            $def['allowDecimals'] = true;
        }
    }

}
