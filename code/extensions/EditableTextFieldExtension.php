<?php


class EditableTextFieldExtension extends DataExtension
{
    private static $db = array(
        'UnitLabel' => 'Varchar(255)',
    );


    public function updateCMSFields(FieldList $fields)
    {
        // Responsive title field for dynamically piping data
        $fields->insertBefore('HelpText',
            TextField::create('UnitLabel', 'Unit Label')
                ->setDescription('Set a unit for the field. (eg. Hectares, Meters, Kilograms)')
        );
    }

    public function updateFieldDefinition(&$def)
    {
        $def['type'] = 'text';

        // Field value length validation
        if(!empty($this->owner->MinLength)) {
            $def['minlength'] = (int)$this->owner->MinLength;
        }
        if(!empty($this->owner->MaxLength)) {
            $def['maxlength'] = (int)$this->owner->MaxLength;
        }

        if(!empty($this->owner->UnitLabel)) {
            $def['unitLabel'] = $this->owner->UnitLabel;
        }

        // Textarea
        if(!empty($this->owner->Rows)) {
            $rows = $this->owner->Rows;
            if ($rows > 1) {
                $def['rows'] = $rows;
                $def['type'] = 'textarea';
            }
        }
    }

}