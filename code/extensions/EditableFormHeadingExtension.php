<?php


class EditableFormHeadingExtension extends DataExtension
{

    public function updateFieldDefinition(&$def)
    {
        $def['type'] = 'placeholder';
    }

}