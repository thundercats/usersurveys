<?php


class EditableMultipleOptionFieldExtension extends DataExtension
{

    public function updateFieldDefinition(&$def)
    {
        // Options
        $def['options'] = array();
        foreach($this->owner->Options() as $option) {
            /* @var $option EditableOption */
            $def['options'][] = array(
                'label' => $option->Title,
                'value' => $option->Value,
            );
        }

        // Default values
        foreach($this->owner->Options()->filter('Default', 1) as $option) {
            $def['default'] = $option->Value;
        }
    }

}