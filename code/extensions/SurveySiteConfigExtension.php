<?php

class SurveySiteConfigExtension extends DataExtension
{
    private static $db = array(
        'SurveyLegalObligations' => 'HTMLText',
        'SurveyPrivacyAndConfidentiality' => 'HTMLText',
    );

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.Survey', array(
             HTMLEditorField::create('SurveyLegalObligations')->setRows(5),
             HTMLEditorField::create('SurveyPrivacyAndConfidentiality')->setRows(5),
         ));
    }
}
