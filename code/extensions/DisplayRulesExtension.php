<?php

/**
 * Applied to questions and sections to apply display logic
 */
class DisplayRulesExtension extends DataExtension
{

    private static $db = [
        'DisplayRuleAction' => 'Enum("Show, Hide")',
        'InitialVisibility' => 'Boolean(1)',
    ];

    private static $has_many = [
        'UserSurveysDisplayRules' => 'UserSurveysDisplayRule.Parent',
    ];

    private static $defaults = [
        'InitialVisibility' => 1,
    ];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->removeFieldsFromTab('Root.DisplayRules', [
            'DisplayRules'
        ]);
        $fields->removeByName('UserSurveysDisplayRules');

        $fields->addFieldsToTab('Root.DisplayRules', [
            CheckboxField::create('InitialVisibility', 'Show on load'),
            DropdownField::create(
                'DisplayRuleAction',
                'When any display rule is true, this field will',
                $this->owner->dbObject('DisplayRuleAction')->enumValues()
            )->setEmptyString(''),
        ]);

        if($this->owner->ID) {
            $rules = GridField::create(
                'UserSurveysDisplayRules',
                'Display Rules',
                $this->owner->UserSurveysDisplayRules(),
                GridFieldConfig_RecordEditor::create()
            );
            $fields->addFieldToTab('Root.DisplayRules', $rules);
        }
    }

    public function updateFieldDefinition(&$def)
    {
        $action = $this->owner->DisplayRuleAction;
        if(!$action || $this->owner->UserSurveysDisplayRules()->count() < 1) {
            return;
        }

        $displayRule = [
            'action' => $this->owner->DisplayRuleAction,
            'controlFields' => [],
        ];

        foreach($this->owner->UserSurveysDisplayRules() as $rule) {
            $ruleDef = $rule->getFieldDefinition();
            if(!empty($ruleDef)) {
                $displayRule['controlFields'][] = $ruleDef;
            }
        }

        if(count($displayRule['controlFields']) > 0) {
            $def['displayRule'] = $displayRule;
        }

        $def['showOnLoad'] = (boolean) $this->owner->InitialVisibility;
    }

}
