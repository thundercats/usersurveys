<?php

class EditableFormFieldExtension extends DataExtension implements UserSurveysFormField
{
    private static $db = array(
        'ResponsiveTitle' => 'Varchar(255)',
        'Description' => 'HTMLText',
        'HelpText' => 'HTMLText',
        'QuestionNumber' => 'Varchar(50)',
    );

    private static $has_one = array(
        'Section' => 'Section', // null if we are nested under a group
        'EditableFormFieldGroup' => 'EditableFormFieldGroup', // null if we are directly under a section
    );

    private static $summary_fields = array(
        'Type',
        'Name',
        'Title',
    );

    public function updateCMSFields(FieldList $fields)
    {
        $fields->AddFieldsToTab('Root.Main', array(
            TextField::create('QuestionNumber', 'Question number')
                ->setMaxLength(3)
                ->setDescription('(Optional)'),
            HTMLEditorField::create('HelpText')
                ->setRows(15),
            HTMLEditorField::create('Description', 'Question note')
                ->setRows(20),
        ));

        // Responsive title field for dynamically piping data
        $fields->insertAfter('Title',
            TextField::create('ResponsiveTitle', 'Responsive Title')
                ->setDescription('Responsive title is used to set the title based on the response of previous questions. (eg. Hello, [NameField])')
        );

        // Remove configuration options we aren't implementing
        $fields->removeFieldsFromTab('Root.Main', array(
            'Default',
            'Placeholder',
            'ExtraClass',
            'RightTitle',
            'Type', // we allow type to be changed
            'MergeField', // we don't use merge fields
        ));

        $fields->removeFieldsFromTab('Root.DisplayRules', array(
            'ShowOnLoad',
        ));

        $classDropdown = DropdownField::create(
            'ClassName',
            'Type',
            $this->getSupportedClassTypes(),
            get_class($this->owner)
        );

        // Because we haven't saved, we don't have a parent. This means we can't
        // check allowed children (it returns all classes). We turn this into a readonly
        // field until the field has been saved.
        if(!$this->owner->ID) {
            $classDropdown = ReadonlyField::create(
                'ClassName',
                'Type',
                get_class($this->owner)
            );
        }

        $fields->insertBefore('Title', $classDropdown);
    }

    public function validate(ValidationResult $validationResult)
    {
        if ($this->owner->SectionID && $this->owner->EditableFormFieldGroupID) {
            // A field can only ever have a section or a group as a parent, never both. This should never happen
            // with how gridfield works, but just in case, we check for it.
            $validationResult->error('Invalid relationship detected. Please contact a developer or delete and re-create this field.');
        }
    }

    public function getFormField() {
        // since we are not using SS templates, we don't need proper form fields
        return new FormField();
    }

    /**
     * @return array of subtype class name => human readable name
     */
    protected function getSupportedClassTypes()
    {
        $parent = $this->getParent();
        if($parent && $parent->hasMethod('getSupportedChildrenTypes')) {
            return $parent->getSupportedChildrenTypes();
        }

        return $this->owner->getEditableFieldClasses();
    }

    public function getFieldDefinitionType()
    {
        return Config::inst()->get(get_class($this->owner), 'field_type');
    }

    public function getLongTitle()
    {
        return sprintf('%s // %s', $this->owner->Name, $this->owner->Title);
    }

    public function getFieldDefinition()
    {
        $field = array(
            'id' => $this->owner->Name,
            'label' => $this->owner->Title,
            'responsiveLabel' => $this->owner->ResponsiveTitle,
            'type' => $this->getFieldDefinitionType(),
        );

        // Required field validation
        if($this->owner->Required) {
            $field['mandatory'] = true;
        }

        if(!empty($this->owner->getErrorMessage())) {
            $field['errorText'] = $this->owner->getErrorMessage()->HTML();
        }

        if(!empty($this->owner->Description)) {
            $field['noteContent'] = $this->owner->Description;
        }

        // Show on laod
        if(!empty($this->owner->InitialVisibility)) {
            $field['showOnLoad'] = (boolean)$this->owner->InitialVisibility;
        }
        else {
            $field['showOnLoad'] = false;
        }

        if(!empty($this->owner->HelpText)) {
            $field['helpText'] = $this->owner->HelpText;
        }

        if(!empty($this->owner->QuestionNumber)) {
            $field['questionNumber'] = $this->owner->QuestionNumber;
        }

        $this->owner->extend('updateFieldDefinition', $field);

        return $field;
    }

    public function Type() {
        return $this->owner->i18n_singular_name();
    }

    /**
     * @return SurveyUserDefinedForm|null
     */
    public function getForm() {
        if (!$this->owner->ID) {
            // Due to the difficulty of getting the parent page ID during objection creation, we wait until
            // after it is first saved to be able to get the parent form
            return null;
        }

        if($this->getParent()) {
            return $this->getParent()->getForm();
        }

        return null;
    }

    /**
     * @return UserSurveysFormField|null
     */
    public function getParent()
    {
        if($this->owner->SectionID) {
            return $this->owner->Section();
        }

        if($this->owner->EditableFormFieldGroupID) {
            return $this->owner->EditableFormFieldGroup();
        }

        return null;
    }

    /**
     * @return string - name of this field, prefixed by parent names using dot notation
     */
    public function getKey() {
        if ($this->owner instanceof EditableFormField) {

            $prefix = '';
            $parent = $this->owner->getParent();
            if ($parent && $parent instanceof EditableFormField) {
                $parentKey = $parent->getKey();
                if ($parentKey) {
                     $prefix = $parentKey . '.';
                }
            }

            return $prefix . $this->owner->Name;

        } else if ($this->owner instanceof Section) {
            return '';
        }
    }

}
