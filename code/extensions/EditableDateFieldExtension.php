<?php


class EditableDateFieldExtension extends DataExtension
{

    public function updateFieldDefinition(&$def)
    {
        $def['type'] = 'date';
    }

}