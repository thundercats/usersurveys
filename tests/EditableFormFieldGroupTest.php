<?php

class EditableFormFieldGroupTest extends SapphireTest
{

    public function test_getFieldDefinition()
    {
        $groupField = new EditableFormFieldGroup();
        $groupField->Name = 'testgroup';
        $groupField->Title = 'Test group field';

        $def = $groupField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'testgroup',
            'label' => 'Test group field',
            'type' => 'composite',
            'fields' => array(),
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
            'fieldDisplayFormat' => '',
        ), $def);

        $textField1 = EditableTextField::create();
        $textField1->Name = 'child1';
        $textField2 = EditableTextField::create();
        $textField2->Name = 'child2';

        $groupField->ChildFields()->add($textField1);
        $groupField->ChildFields()->add($textField2);

        $def = $groupField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'testgroup',
            'label' => 'Test group field',
            'type' => 'composite',
            'fields' => array(
                [
                    'type' => 'text',
                    'id' => 'child1',
                    'label' => null,
                    'responsiveLabel' => null,
                    'errorText' => 'This is a required field.',
                    'showOnLoad' => true,
                ],
                [
                    'type' => 'text',
                    'id' => 'child2',
                    'label' => null,
                    'responsiveLabel' => null,
                    'errorText' => 'This is a required field.',
                    'showOnLoad' => true,
                ]
            ),
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
            'fieldDisplayFormat' => '',
        ), $def);
    }
}
