<?php

class ThreeColumnTableFieldTest extends SapphireTest
{

    public function testHasHeader()
    {
        $table = new ThreeColumnTableField();
        $this->assertFalse($table->hasHeader());

        $table->Header3 = ' ';
        $this->assertFalse($table->hasHeader());

        $table->Header3 = 'Test';
        $this->assertTrue($table->hasHeader());
    }

    public function testGetColumnCount()
    {
        $table = new ThreeColumnTableField();
        $this->assertSame(3, $table->getColumnCount());
    }
}

