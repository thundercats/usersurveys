<?php

class FormFieldDefinitionTest extends SapphireTest
{

    public function test_TextField_getFieldDefinition()
    {
        // Test text field
        $textField = EditableTextField::create();
        $textField->Name = 'textField';
        $textField->Title = 'Test text field';

        $def = $textField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'textField',
            'label' => 'Test text field',
            'type' => 'text',
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
        ), $def);

        // Test text field with minlegnth, maxlength, helptext and is required
        $textFieldWithDefault = EditableTextField::create();
        $textFieldWithDefault->Name = 'textField';
        $textFieldWithDefault->Title = 'Test text field';
        $textFieldWithDefault->HelpText = 'This is a helptext';
        $textFieldWithDefault->Required = true;
        $textFieldWithDefault->MinLength = 2;
        $textFieldWithDefault->MaxLength = 12;
        $textFieldWithDefault->UnitLabel = 'Units!';

        $def = $textFieldWithDefault->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'textField',
            'label' => 'Test text field',
            'type' => 'text',
            'mandatory' => true,
            'minlength' => 2,
            'maxlength' => 12,
            'helpText' => 'This is a helptext',
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
            'unitLabel' => 'Units!'
        ), $def);
    }

    public function test_NumericField_getFieldDefinition()
    {
        // Test simple numeric
        $numericField = EditableNumericField::create();
        $numericField->Name = 'numericField';
        $numericField->Title = 'Test numeric field';

        $def = $numericField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'numericField',
            'label' => 'Test numeric field',
            'type' => 'text-number',
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
        ), $def);

        // Test numeric's max and min value
        $numericField = EditableNumericField::create();
        $numericField->Name = 'numericField';
        $numericField->Title = 'Test numeric field';
        $numericField->MinValue = 2;
        $numericField->MaxValue = 100;
        $numericField->UnitLabel = 'Units!';

        $def = $numericField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'numericField',
            'label' => 'Test numeric field',
            'type' => 'text-number',
            'min' => 2,
            'max' => 100,
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
            'unitLabel' => 'Units!'
        ), $def);

        // Test numeric's min value with zero input
        $numericField->MinValue = 0;
        $def = $numericField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'numericField',
            'label' => 'Test numeric field',
            'type' => 'text-number',
            'min' => 0,
            'max' => 100,
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
            'unitLabel' => 'Units!'
        ), $def);
    }

    public function test_RadioField_getFieldDefinition()
    {
        // Test radio field
        $optionSetField = UserSurveysEditableRadioField::create();
        $optionSetField->Name = 'optionSetField';
        $optionSetField->Title = 'This is an option set field';

        $opt1 = new EditableOption();
        $opt1->Title = 'One';
        $opt1->Value = 'one';
        $opt2 = new EditableOption();
        $opt2->Title = 'Two';
        $opt2->Value = 'two';
        $opt3 = new EditableOption();
        $opt3->Title = 'Three';
        $opt3->Value = 'three';

        $optionSetField->Options()->add($opt1);
        $optionSetField->Options()->add($opt2);
        $optionSetField->Options()->add($opt3);
        $optionSetField->write(); // prevent LogicException: filter can't be called on an UnsavedRelationList

        $def = $optionSetField->getFieldDefinition();
        $this->assertEquals(array(
            'id' => 'optionSetField',
            'label' => 'This is an option set field',
            'type' => 'radio',
            'options' => array(
                array('label' => 'One', 'value' => 'one'),
                array('label' => 'Two', 'value' => 'two'),
                array('label' => 'Three', 'value' => 'three'),
            ),
            'responsiveLabel' => null,
            'errorText' => 'This is a required field.',
            'showOnLoad' => true,
        ), $def);

        //
    }
}
