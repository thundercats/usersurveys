<?php

class TwoColumnTableFieldTest extends SapphireTest
{

    public function testHasHeader()
    {
        $table = new TwoColumnTableField();
        $this->assertFalse($table->hasHeader());

        $table->Header1 = 'Test';
        $this->assertTrue($table->hasHeader());

        $table->Header1 = '';
        $this->assertFalse($table->hasHeader());

        $table->Header2 = ' ';
        $this->assertFalse($table->hasHeader());

        $table->Header2 = 'Test';
        $this->assertTrue($table->hasHeader());
    }

    public function testGetColumnCount()
    {
        $table = new TwoColumnTableField();
        $this->assertSame(2, $table->getColumnCount());
    }

    public function testHasTotalsRow()
    {
        $table = new TwoColumnTableField();
        $this->assertFalse($table->hasTotalsRow());

        $table->TotalsLabel = 'Label';
        $this->assertFalse($table->hasTotalsRow());

        $table->Column2UnitLabel = 'Unit Label';
        $this->assertFalse($table->hasTotalsRow());

        $table->Column2Calculate = true;
        $this->assertTrue($table->hasTotalsRow());

        $table->Column2UnitLabel = '';
        $this->assertTrue($table->hasTotalsRow());

        $table->TotalsLabel = '';
        $this->assertTrue($table->hasTotalsRow());
    }

    public function testGetTotalsRow()
    {
        // Test that row totals show with calculate and unit label
        $table = new TwoColumnTableField();
        $table->Column2Calculate = true;
        $table->Column2UnitLabel = 'Units!';
        $expected = [['calculate' => true, 'unitLabel' => 'Units!']];
        $this->assertSame($expected, $table->getTotalsRow());

        // Test that row totals show without unit label when calculate is false,
        // even if unitLabel has a value
        $table = new TwoColumnTableField();
        $table->Column2Calculate = false;
        $table->Column2UnitLabel = 'Units!';
        $expected = [['calculate' => false, 'unitLabel' => null]];
        $this->assertSame($expected, $table->getTotalsRow());

        // Test that unit label is null when its empty
        $table = new TwoColumnTableField();
        $table->Column2Calculate = true;
        $table->Column2UnitLabel = '';
        $expected = [['calculate' => true, 'unitLabel' => null]];
        $this->assertSame($expected, $table->getTotalsRow());
    }
}
