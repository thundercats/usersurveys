<?php

class SurveyContactInformationTest extends SapphireTest
{
    public function test_getFieldDefinition()
    {
        $obj = new SurveyContactInformation();

        $obj->update(array(
            'Title' => 'John',
            'FreePhone' => '021 231 1231',
            'Email' => 'test@test.com',
        ));

        // Test contact with no ID
        $def = $obj->getFieldDefinition();
        $this->assertEquals(
            false,
            $def
        );

        // Test contact with only 2 fields
        $obj->update(array(
            'ID' => 1,
        ));
        $def = $obj->getFieldDefinition();
        $this->assertEquals(
            array(
                'FreePhone' => '021 231 1231',
                'Email' => 'test@test.com',
            ),
            $def
        );

        // Test multi-line values
        $obj->update(array(
            'Mail' => "PO Box 123\nWellington\r\nNew Zealand\rEarth",
        ));
        $def = $obj->getFieldDefinition();
        $this->assertEquals(
            array(
                'FreePhone' => '021 231 1231',
                'Email' => 'test@test.com',
                'Mail' => 'PO Box 123<br>Wellington<br>New Zealand<br>Earth',
            ),
            $def
        );
    }
}
