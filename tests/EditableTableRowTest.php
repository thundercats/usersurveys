<?php

class EditableTableRowTest extends SapphireTest
{
    public function testTwoColumnRow()
    {
        $table = new TwoColumnTableField();
        $table->write();

        $tableRow = new EditableTableRow();
        $tableRow->EditableFormFieldGroupID = $table->ID;
        $tableRow->write();

        // 1 column is taken by the label
        $this->assertSame(1, $tableRow->getCells()->count());
    }

    public function testThreeColumnRow()
    {
        $table = new ThreeColumnTableField();
        $table->write();

        $tableRow = new EditableTableRow();
        $tableRow->EditableFormFieldGroupID = $table->ID;
        $tableRow->write();

        // 1 column is taken by the label
        $this->assertSame(2, $tableRow->getCells()->count());
    }
}
