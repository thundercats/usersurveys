<?php

class SectionTest extends SapphireTest
{
    public function test_getFieldDefinition()
    {
        $headerField = EditableFormHeading::create();
        $headerField->Name = 'child1';
        $textField = EditableTextField::create();
        $textField->Name = 'child2';

        $section = Section::create();
        $section->Title = 'Section title';
        $section->Questions()->add($headerField);
        $section->Questions()->add($textField);

        $def = $section->getFieldDefinition();
        $this->assertEquals(
            array(
                'title' => 'Section title',
                'questions' => array(
                    $headerField->getFieldDefinition(),
                    $textField->getFieldDefinition(),
                ),
                'showOnLoad' => true,
                'introduction' => '',
            ), $def);

        // Test empty question array
        $section->Questions()->removeAll(); // Reset children
        $def = $section->getFieldDefinition();
        $this->assertEquals(
            array(
                'title' => 'Section title',
                'questions' => array(),
                'showOnLoad' => true,
                'introduction' => '',
            ), $def);
    }
}
