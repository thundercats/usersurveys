# Survey User Forms - Publish to JSON Module

## Documentation

This module is an extension of the userforms module and will publish the form data as a JSON file instead of using the default form submission.

## Requirements

Refer to `composer.json` file.

## Installation

Clone module from https://gitlab.cwp.govt.nz/thundercats/usersurveys.git into your project root directory.
