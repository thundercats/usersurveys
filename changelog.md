# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0]

* First working version
* Changelog added
* README added
* Added git ignore
* Added standard git attributes
* Added standard license
* Added standard editor config