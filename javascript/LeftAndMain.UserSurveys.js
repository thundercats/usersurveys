jQuery.noConflict();

/**
 * File: LeftAndMain.UserSurveys.js
 * Extends Framework LeftAndMain.js
 */
(function($) {

	// setup jquery.entwine
	$.entwine.warningLevel = $.entwine.WARN_LEVEL_BESTPRACTISE;
	$.entwine('ss', function($) {

		/**
		 * Add styling to all contained buttons, and create buttonsets if required.
		 */
		$('.cms-content .Actions').entwine({
			onmatch: function() {
				this._super();

				this.find('.ss-ui-button').click(function() {
					form = this.form;

					// forms don't natively store the button they've been triggered with
					if(form) {
						form.clickedButton = this;

						// Reset the clicked button shortly after the onsubmit handlers
						// have fired on the form
						setTimeout(function() {
							// disable the save button to prevent multiple save requests
							$(form.clickedButton).attr('disabled', true);
						}, 10);
					}

				});

			}

		});
	});

}(jQuery));
